BTV
===

Installation
------------

``git clone https://gitlab.cern.ch/cms-phys-ciemat/btv-corrections.git Corrections/BTV``

RDFModules
----------

.. toctree::
    :maxdepth: 3

    btagging 
